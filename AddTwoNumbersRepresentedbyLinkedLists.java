//{ Driver Code Starts
// driver

import java.util.*;

class Node {
    int data;
    Node next;

    Node(int d) {
        data = d;
        next = null;
    }
}

class GfG{
    
    static void printList(Node n){
        while(n!=null){
            System.out.print(n.data+" ");
            n = n.next;
        }
        System.out.println();
    }
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int T = sc.nextInt();
        
        while (T-- > 0) {
            
            int n = sc.nextInt();
            int val = sc.nextInt();
            
            Node first = new Node(val);
            Node tail = first;
            for(int i=0; i<n-1; i++)
            {
                val = sc.nextInt();
                tail.next = new Node(val);
                tail = tail.next;
            }
            
            int m = sc.nextInt();
            val = sc.nextInt();
            
            Node second = new Node(val);
            tail = second;
            for(int i=0; i<m-1; i++)
            {
                val = sc.nextInt();
                tail.next = new Node(val);
                tail = tail.next;
            }
            
            Solution g = new Solution();
            Node res = g.addTwoLists(first, second);
            printList(res);
        }
    }
}

// } Driver Code Ends


/* node for linked list

class Node {
    int data;
    Node next;

    Node(int d) {
        data = d;
        next = null;
    }
}

*/

class Solution {
    // Function to reverse a linked list.
    static Node reverseList(Node head) {
        Node prev = null;
        Node current = head;
        Node next = null;
        while (current != null) {
            next = current.next;
            current.next = prev;
            prev = current;
            current = next;
        }
        return prev;
    }

    // Function to add two numbers represented by linked list.
    static Node addTwoLists(Node first, Node second) {
        // Reverse both input linked lists.
        first = reverseList(first);
        second = reverseList(second);

        Node dummyHead = new Node(0);
        Node curr = dummyHead;
        int carry = 0;

        // Traverse both reversed linked lists until both are null.
        while (first != null || second != null) {
            // Get the values of the current nodes, or 0 if the node is null.
            int val1 = (first != null) ? first.data : 0;
            int val2 = (second != null) ? second.data : 0;

            // Calculate the sum of current digits and carry.
            int sum = val1 + val2 + carry;

            // Update carry for next calculation.
            carry = sum / 10;

            // Update current node's data with the sum of current digits.
            curr.next = new Node(sum % 10);
            curr = curr.next;

            // Move to the next nodes in both lists if they exist.
            if (first != null) first = first.next;
            if (second != null) second = second.next;
        }

        // If there's still a carry, create a new node for it.
        if (carry > 0) {
            curr.next = new Node(carry);
        }

        // Reverse the result linked list and return its head.
        return reverseList(dummyHead.next);
    }
}
